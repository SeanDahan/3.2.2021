#include <iostream>
#include "MessageSender.h"
#include <thread>

#define INFILEPATH "data.txt"
#define OUTFILEPATH "output.txt"
void main()
{
    MessageSender msg(INFILEPATH, OUTFILEPATH);
    std::thread thr1(&MessageSender::FileReader, std::ref(msg));
    std::thread thr2(&MessageSender::FileWriter, std::ref(msg));
    thr1.detach();
    thr2.detach();
    msg.Main();
    msg.ShowMessages();
    
}