#include "MessageSender.h"
MessageSender::MessageSender(std::string fileToReadFrom, std::string fileToWrite)
{
    _inFilePath = fileToReadFrom;
    _outFilePath = fileToWrite;
    //_fileIn.open(fileToReadFrom, std::ios::in);
    //_fileOut.open(fileToWrite, std::ios_base::app);
}

MessageSender::~MessageSender()
{
    _fileIn.close();
    _fileOut.close();
}

void MessageSender::Main()
{
    int choice = 0;
    bool flag = false;
    while (true)
    {
        flag = false;
        choice = 0;
        while (choice < 1 || choice > 4)
        {
            if (flag)
            {
                sc(BRIGHTRED);
                std::cout << " { Error : Unknown Option }\n";
            }
            Menu();
            std::cin >> choice;
            flag = true;
        }
        switch (choice)
        {
        case 1:
            SignIn();
            break;
        case 2:
            SignOut();
            break;
        case 3:
            ShowConnected();
            break;
        case 4:
            //return;
            exit(0);
        }
    }
}

void MessageSender::SignIn()
{
    std::string userName;
    bool flag = true;
    while (true)
    {
        sc(BRIGHTYELLOW);
        std::cout << "[ Enter your username to sign in / 0 to exit ]\n";
        std::cin >> userName;
        if (userName == "0") return;
        std::unique_lock<std::mutex> lckUsers(_mUsers);
        for (std::string user : _connectedUsers)
        {
            if (user == userName)
            {
                sc(BRIGHTRED);
                std::cout << " [ Error : User already exists in the system! ]\n";
                flag = false;
                lckUsers.unlock(); 
                break;
            }
        }
        if (flag)
        {
            _connectedUsers.push_back(userName);
            lckUsers.unlock();
            return;
        }  
        flag = true;
    }  
}

void MessageSender::SignOut()
{
    std::string userName;
    int counter = 0;
    while (true)
    {
        sc(BRIGHTYELLOW);
        std::cout << "[ Enter your username to sign out / 0 to exit ]\n";
        std::cin >> userName;
        if (userName == "0") return;
        std::unique_lock<std::mutex> lckUsers(_mUsers);
        for (auto it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
        {
            if (*it == userName)
            {
                _connectedUsers.erase(it);
                lckUsers.unlock();
                return;
            }
        }
        sc(BRIGHTRED);
        std::cout << " [ Error : User doesn't exist in the system! ]\n";
        lckUsers.unlock();
    } 
}

void MessageSender::ShowConnected()
{
    if (_connectedUsers.size() > 0)
    {
        sc(BRIGHTBLUE);
        std::cout << "> Connected Users <\n";
        sc(BRIGHTGREEN);
        std::unique_lock<std::mutex> lckUsers(_mUsers);
        for (std::string user : _connectedUsers)
        {
            std::cout << " [ " << user << " ]\n";
        }
        lckUsers.unlock();
        sc(BRIGHTWHITE);   
    }
    else
    {
        sc(BRIGHTBLUE);
        std::cout << "> Sorry there are no connected users at the time <\n";
        sc(BRIGHTWHITE);
    }
    system("pause");
    
}

void MessageSender::Menu()
{
    system("cls");
    sc(BRIGHTAQUA);
    std::cout << " << Welcome to Message Sender - By Sean Dahan >>\n";
    sc(BRIGHTPINK);
    std::cout << "[ 1 . Signin ]\n";
    std::cout << "[ 2 . Signout ]\n";
    std::cout << "[ 3 . Connected Users ]\n";
    std::cout << "[ 4 . Exit ]\n";
    std::cout << "< Please enter your selection >\n";
    sc(WHITE);
}

void MessageSender::sc(int id)
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, id);
}

void MessageSender::ShowMessages()
{
    std::unique_lock<std::mutex> lckMessages(_mMessages);
    if (_messageQueue.size() > 0)
    {
        sc(AQUA);
        std::queue<std::string> tmp_q = _messageQueue;
        std::string q_element;
        while (!tmp_q.empty())
        {
            q_element = tmp_q.front();
            //std::cout << "* " << q_element << " *\n";
            tmp_q.pop();
        }
    }
    lckMessages.unlock();
}

void MessageSender::FileReader()
{
    Sleep(10000);
    std::string line;
    while (true)
    {
        _fileIn.open(_inFilePath, std::ios::in);
        if (_fileIn.peek() != std::ifstream::traits_type::eof())
        {
            while (getline(_fileIn, line))
            {
                std::unique_lock<std::mutex> lckMessages(_mMessages);
                _messageQueue.push(line);
                lckMessages.unlock();
            }
            _cvMessages.notify_all();
            _fileIn.close();
            std::ofstream oFile(_inFilePath, std::ofstream::out | std::ofstream::trunc);
            oFile.close();
        }
        _fileIn.close();
        std::this_thread::sleep_for(std::chrono::seconds(6)); 
    }
}

void MessageSender::FileWriter()
{
    std::unique_lock<std::mutex> lckUsers(_mUsers, std::defer_lock);
    while (true)
    {
        std::unique_lock<std::mutex> lckMessages(_mMessages);
        if (_messageQueue.empty())
            _cvMessages.wait(lckMessages);


        lckMessages.unlock();
       
        _fileOut.open(_outFilePath, std::ios_base::app);
        std::string q_element;
        lckMessages.lock();
        while (!_messageQueue.empty() && !_connectedUsers.empty())
        {   
            
            q_element = _messageQueue.front();
            _messageQueue.pop();
            lckMessages.unlock();
            lckUsers.lock();
            for (int i = 0; i < _connectedUsers.size(); i++)
            {
                _fileOut << _connectedUsers[i] << " << " << q_element << "\n";
            }
            lckUsers.unlock();
            lckMessages.lock();
        }
        
        lckMessages.unlock();
        _fileOut.close();
    }
}

