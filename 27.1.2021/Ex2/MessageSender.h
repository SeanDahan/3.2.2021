#pragma once
#include <Windows.h>
#include <iostream>
#include <queue>
#include <map>
#include <fstream>
#include <string>
#include <thread>
#include <mutex>
#define BLACK 0
#define DARKBLUE 1
#define GREEN 2
#define AQUA 3
#define RED 4
#define PINK 5
#define YELLOW 6
#define WHITE 7
#define GRAY 8
#define BRIGHTBLUE 9
#define BRIGHTGREEN 10
#define BRIGHTAQUA 11
#define BRIGHTRED 12
#define BRIGHTPINK 13
#define BRIGHTYELLOW 14
#define BRIGHTWHITE 15

class MessageSender
{
public:
	//C'tor
	MessageSender(std::string fileToReadFrom, std::string fileToWrite);
	
	//D'tor
	virtual ~MessageSender();
	
	void Main();

	//Methods
	void SignIn();
	void SignOut();
	void ShowConnected();


	//UI
	void Menu();

	//Helpers
	void sc(int id);
	void ShowMessages();

	//Thread
	void FileReader();
	void FileWriter();



private:
	std::string _inFilePath;
	std::string _outFilePath;
	std::ifstream _fileIn;
	std::ofstream _fileOut;
	std::mutex _mUsers;
	std::mutex _mMessages;
	std::condition_variable _cvMessages;
	std::queue<std::string> _messageQueue;
	std::vector<std::string> _connectedUsers;
};

