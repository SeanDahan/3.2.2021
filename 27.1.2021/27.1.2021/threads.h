#pragma once
#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
void writePrimesToFile(int begin, int end, std::ofstream& file);
