#include "threads.h"
std::mutex mtx;
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool flag = true;
	for (size_t j = begin; j < end; j++)
	{
		flag = true;
		for (size_t i = 2; i <= j / 2; ++i)
		{
			if (j % i == 0)
			{
				flag = false;
			}
		}
		if (flag)
		{
			mtx.lock();
			std::cout << "Writing " << j << "\n";
			file << j << "\n";
			mtx.unlock();
		}
	}

}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file;
	file.open(filePath, std::ios::out);


	clock_t start, endTime;
	start = clock();
	int tempEnd = begin + end / N;
	for (size_t i = begin; i < end; i += end / N)
	{

		tempEnd = i + end / N;
		std::thread thr(writePrimesToFile, i, tempEnd, std::ref(file));
		thr.join();

	}
	endTime = clock();
	std::cout << "Total time taken " << ((double)(endTime - start)) / CLOCKS_PER_SEC << " Sec" << std::endl;

}